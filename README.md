

Bardarbunga

- https://doi.org/10.1126/science.aaf8988
- first implementation of the BO method not yet named Grond
- application to ~ 100 caldera collapse events
- station corrections 
- classic bootstrap
- no excentricity compensation
- used surface waves
- BB stations over island
- Mw range: 

---

Nuke

- https://doi.org/10.1785/0220160139
- investigate trade-off between +ISO and -CLVD
- used surface waves
- BB stations Asia
- Mw range: 

---

Halle/Leipzig
https://doi.org/10.1007/s10950-018-9746-9
- Body-wave
- Mw range: 

---

Eifel

https://doi.org/10.1093/gji/ggy532

---

Supp. to Eifel

https://doi.org/10.5880/GFZ.2.1.2019.001

---

East African Rift -> supplementary info S2

https://doi.org/10.1029/2018gl080866

---

Anak Krakatau

https://doi.org/10.1038/s41467-019-12284-5

---

Mayotte

https://doi.org/10.1038/s41561-019-0505-5

---

Groningen Part 1

https://doi.org/10.1785/0120200099

---

Groningen Part 2

https://doi.org/10.1785/0120200076

---

Qaidam

https://doi.org/10.3390/rs12172850

---

Atatürk

https://doi.org/10.3389/feart.2021.663385

---

Montesano Earthquake

https://doi.org/10.3389/feart.2020.617794

---

Alps

- https://doi.org/10.5194/se-12-1233-2021
- tests of different input datasets (frequency-domain, time-domain, cross-correlation)
- testing different frequency bands: surface waves and body waves
- testing influence of azimuthal station distributions
- testing of reliability of non-DC components
- Mw range: 3.1-4.7
---

Segmentation

https://doi.org/10.1093/gji/ggaa351

---

PEGS

https://doi.org/10.1016/j.epsl.2020.116150

---

Swarm earthquakes West Bohemia 2018

- Comparison with Qopen method
- https://arxiv.org/abs/2107.11083
- Report at https://data.pyrocko.org/publications/grond-reports/west-bohemia-2018/
- Mw 1.8 - 3.6
- 148 events
- P and S body waves
- Distances < 25 km

---

Reykjavik geothermal soft stimulation

- https://doi.org/10.1016/j.geothermics.2021.102146
- Mw -1.0 - 0.0
- 70 events - 23 stable
- Details in supplement https://ars.els-cdn.com/content/image/1-s2.0-S0375650521001061-mmc8.docx
- picked phase travel-times 
- waveforms in TD 5 - 15 Hz

---

Stress Chatter via Fluid Flow and Fault Slip in a Hydraulic Fracturing-Induced Earthquake Sequence in the Montney Formation, British Columbia

- https://doi.org/10.1029/2020GL087254
- 5 events
- Mw 1.8 - 4.1
- TD + FD + envelope
- full MTs
- Distances < 50 km
- Description in supplement https://agupubs.onlinelibrary.wiley.com/action/downloadSupplement?doi=10.1029%2F2020GL087254&file=grl60893-sup-0001-Supplementary+mateials.pdf

---

The 2014 Juan Fernández microplate earthquake doublet: Evidence for large thrust faulting driven by microplate rotation

https://doi.org/10.1016/j.tecto.2021.228720

---

On rapid multidisciplinary response aspects for Samos 2020 M7.0 earthquake

- https://doi.org/10.1007/s11600-021-00578-6
- Mw 7.0
- DInSAR
- GNSS
- Rectangular fault model

---

Slip Model of the 2020 Yutian (Northwestern Tibetan Plateau) Earthquake Derived From Joint Inversion of InSAR and Teleseismic Data

- https://doi.org/10.1029/2020EA001409
- used for their initial rectangular model
- Insar 
- body-waves 0.01 - 0.05 Hz sampled at 5 Hz (?)

---

Variations in the Seismogenic Thickness of East Africa

https://doi.org/10.1029/2020JB020754

---

Focal Parameters of Earthquakes Offshore Cape St. Vincent Using an Amphibious Network
https://doi.org/10.1007/s00024-020-02475-3

---

Yield estimation of the 2020 Beirut explosion using open access waveform and remote sensing data
https://doi.org/10.1038/s41598-021-93690-y

---

Two Decades of Seismicity in Storfjorden, Svalbard Archipelago, from Regional Data

- https://doi.org/10.1785/0220200469
- 4 events
- deviatoric MT
- Mw 4.1 - 5.1
- Surface waves

---

Seismicity at the Castor gas reservoir driven by pore pressure diffusion and asperities loading 

- https://doi.org/10.1038/s41467-021-24949-1
- Deviatoric MTI
- Surface waves at 0.04 - 0.10 Hz
- CC for stations < 350 km
- TD for stations < 100 km
- FD for stations < 350 km
- 1 OBS station included with CC

---

Kinematics of Active Deformation in the Malawi Rift and Rungwe Volcanic Province, Africa
- https://doi.org/10.1029/2019GC008354
- Full MTs
- Two events ML 5.2 and 4.4
- Surface waves

---

Focal Parameters of Earthquakes Offshore Cape St. Vincent Using an Amphibious Network

- https://doi.org/10.1007/s00024-020-02475-3
- FD < 350 km
- CC < 350 km, including OBSs
- P and S waveforms 0.5 - 3.0 (2.5) Hz
- 7 events
- Mw 3.2 - 4.2

